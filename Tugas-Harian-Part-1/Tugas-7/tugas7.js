// Soal 1
let dataPeserta = ["john", "laki-laki", "programmer", "30"]

const [nama, gender, pekerjaan, umur] = dataPeserta

let output = 'Halo, nama saya ' + nama + '. Saya ' + gender + ' berumur ' + umur + ' bekerja sebagai seorang ' + pekerjaan;

console.log(output)


// Soal 2
let array1 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"]

for(let i = 0; i < array1.length; i++){
    console.log(array1[i])
}

// Soal 3
let array2 = [1, 2, 3, 4, 5, 6,7, 8, 9, 10]

for(let i = 1; i <= 10; i+=2){
    console.log(array2[i])
}

// Soal 4
let kalimat= ["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"]
kalimat.splice(0, 1)
kalimat.splice(1, 1)
const [aku, sangat, senang, belajar, javascript] = kalimat

let hasil = aku + ' ' + sangat + ' ' + senang + ' ' + belajar + ' ' + javascript;

console.log(hasil)


// Soal 5
var sayuran=[]
sayuran.push("Kangkung", "Bayam", "Buncis", "Kubis", "Timun", "Seledri", "Tauge")
sayuran.sort()

let array3 = [1, 2, 3, 4, 5, 6, 7]

for(let i = 0; i < 7; i++){
    console.log(array3[i] + '. ' + sayuran[i])
}
