// Soal 1
var nilaiJohn = 80;
var nilaiDoe = 50; 

if ( nilaiJohn >= 80 ) {
    console.log("Nilai John A")
} else if( nilaiJohn >= 70 & nilaiJohn < 80 ){
    console.log("Nilai John B")
} else if( nilaiJohn >= 60 & nilaiJohn < 70 ){
    console.log("Nilai John C")
} else if( nilaiJohn >= 50 & nilaiJohn < 60 ){
    console.log("Nilai John D")
} else {
    console.log("Nilai John E")
}

if ( nilaiDoe >= 80 ) {
    console.log("Nilai Doe A")
} else if( nilaiDoe >= 70 & nilaiDoe < 80 ){
    console.log("Nilai Doe B")
} else if( nilaiDoe >= 60 & nilaiDoe < 70 ){
    console.log("Nilai Doe C")
} else if( nilaiDoe >= 50 & nilaiDoe < 60 ){
    console.log("Nilai Doe D")
} else {
    console.log("Nilai Doe E")
}

// Soal 2
var tanggal = 5;
var bulan = 5;
var tahun = 1993;
switch(bulan) {
  case 1:   { console.log(tanggal +' Januari '+ tahun); break; }
  case 2:   { console.log(tanggal +' Februari '+ tahun); break; }
  case 3:   { console.log(tanggal +' Maret '+ tahun); break; }
  case 4:   { console.log(tanggal +' April '+ tahun); break; }
  case 5:   { console.log(tanggal +' Mei '+ tahun); break; }
  case 6:   { console.log(tanggal +' Juni '+ tahun); break; }
  case 7:   { console.log(tanggal +' Juli '+ tahun); break; }
  case 8:   { console.log(tanggal +' Agustus '+ tahun); break; }
  case 9:   { console.log(tanggal +' September '+ tahun); break; }
  case 10:   { console.log(tanggal +' Oktober '+ tahun); break; }
  case 11:   { console.log(tanggal +' November '+ tahun); break; }
  case 12:   { console.log(tanggal +' Desember '+ tahun); break; }
  default:  { console.log('Kosong'); }}

// Soal 3
console.log('LOOPING PERTAMA');
for(var deret = 2; deret <= 20; deret += 2) {
  console.log(deret + ' - I love coding');
}
 
console.log('LOOPING KEDUA'); 
for(var deret = 20; deret > 0; deret -= 2) {
  console.log(deret +' - I will become a frontend developer');
} 

// Soal 4
for(var i = 1; i <= 15 ; i++){
    if(i % 3 === 0 & i % 2 !== 0){
      console.log(i + " - I Love Coding")
    }else if(i % 2 === 0){
      console.log(i + " - Berkualitas")  
    }else{
      console.log(i + " - Santai")
    }
  }

// Soal 5
var s ='';
for(var i = 0; i < 7; i++){
    for(var j = 0; j <= i; j++) {
        s += '#';
    }
    s += '\n';
}
console.log(s);