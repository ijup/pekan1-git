// Soal 1
/* 
    Tulis kode function di sini
*/
function luasPersegiPanjang(panjang, lebar){
    let L = panjang * lebar;
    return L;
}

function kelilingPersegiPanjang(panjang, lebar){
    let k = 2 * (panjang + lebar);
    return k;
}

function volumeBalok(panjang, lebar, tinggi){
    let V = panjang * lebar * tinggi;
    return V;
}

let panjang= 12
let lebar= 4
let tinggi = 8
 
let HasilluasPersegiPanjang = luasPersegiPanjang(panjang, lebar)
let HasilkelilingPersegiPanjang = kelilingPersegiPanjang(panjang, lebar)
let HasilvolumeBalok = volumeBalok(panjang, lebar, tinggi)

console.log(HasilluasPersegiPanjang ) 
console.log(HasilkelilingPersegiPanjang )
console.log(HasilvolumeBalok )


// Soal 2
/* 
    Tulis kode function di sini
*/
const introduce = (...rest) =>{ 
    let [nama, umur, gender, pekerjaan] = rest
    return ` Pak ${nama} adalah seorang ${pekerjaan} yang berusia ${umur} tahun `
    } 
//kode di bawah ini jangan dirubah atau dihapus
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log(perkenalan) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"


// Soal 3
let arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku" , 1992]
let objDaftarPeserta = {nama: "John Doe", gender: "laki-laki", hobi: "baca buku", tahun: 1992}

console.log(objDaftarPeserta)


// Soal 4
var buah = [{nama: "Nanas", warna: "Kuning", adaBijinya: "tidak", harga: 9000}, 
            {nama: "Jeruk", warna: "Oranye", adaBijinya: "ada", harga: 8000}, 
            {nama: "Semangka", warna: "Hijau & Merah", adaBijinya: "ada", harga: 10000},
            {nama: "Pisang", warna: "Kuning", adaBijinya: "tidak", harga: 5000}]

var arrayBuahFilter = buah.filter(function(item){
   return item.adaBijinya != "tidak";
})

console.log(arrayBuahFilter)


//Soal 5
let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020,
    colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
 }
 // kode diatas ini jangan di rubah atau di hapus sama sekali
 
 /* Tulis kode jawabannya di sini */
 const phoneBrand = phone.brand;
 const phoneName = phone.name;
 const year = phone.year;
 const colorBronze = phone.colors[0];
 const colorWhite = phone.colors[1];
 const colorBlack = phone.colors[2];
 
 // kode di bawah ini jangan dirubah atau dihapus
 console.log(phoneBrand, phoneName, year, colorBlack, colorBronze) 

